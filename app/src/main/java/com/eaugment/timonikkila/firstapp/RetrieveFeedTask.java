package com.eaugment.timonikkila.firstapp;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.mime.MultipartEntityBuilder;
import cz.msebera.android.httpclient.entity.mime.content.ByteArrayBody;
import cz.msebera.android.httpclient.entity.mime.content.ContentBody;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;

class RetrieveFeedTask extends AsyncTask<String, Void, HttpEntity> {

    private Exception exception;
    private Bitmap template;
    private SpeekItOut speakIt;
    private TextToSpeech tts;


    //https://www.techjini.com/blog/android-speech-to-text-tutorial-part1/

    public RetrieveFeedTask(Bitmap blurTemplate, SpeekItOut speakItParam) {
        template = blurTemplate;
        speakIt = speakItParam;
    }


    @Override
    protected HttpEntity doInBackground(String... strings) {
        try {

            HttpPost post = new HttpPost( "http://192.168.43.150:8000/posts/");
            HttpClient client = HttpClientBuilder.create().build();
            System.out.println("testi13");

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            template.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();

            // ByteArrayBody(byte[] data, ContentType contentType, String filename)
            ContentBody ter = new ByteArrayBody(bitmapdata, ContentType.create("image/png"), "image.png");

            MultipartEntityBuilder mpEntity = MultipartEntityBuilder.create();
            mpEntity.addPart("file", ter);
            HttpEntity entity = mpEntity.build();
            post.setEntity(entity);

            HttpResponse response = client.execute(post);

            System.out.println("testi2");
            System.out.println(response);
            HttpEntity httpEntity = response.getEntity();

            BufferedReader reader = new BufferedReader(new InputStreamReader(httpEntity.getContent(), "UTF-8"));
            String json = reader.readLine();
            System.out.println("testi26");
            System.out.println(json);

            speakIt.onInit(TextToSpeech.SUCCESS);
            //httpEntity.getContent();
            //System.out.println(httpEntity);

            return httpEntity;

        } catch (Exception e) {
            this.exception = e;
            System.out.println("testi9");
            System.out.println(e);


            return null;
        }
    }

}
