package com.eaugment.timonikkila.firstapp;


//https://stackoverflow.com/questions/24693420/text-to-speech-library-android?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.Locale;

public class SpeekItOut extends Activity implements
        TextToSpeech.OnInitListener {
    /** Called when the activity is first created. */

    private TextToSpeech tts;
    private Context context;

    public SpeekItOut(Context cnt) {
        context = cnt;
        tts = new TextToSpeech(context, this);
    }

    @Override
    public void onInit(int status) {
        System.out.println("testi42");

        if (status == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(Locale.US);
            System.out.println("testi46");
            System.out.println(tts.getVoices());
            tts.setPitch(0.8f); // set pitch level
            tts.setSpeechRate(0.4f); // set speech speed rate



            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "Language is not supported");
            } else {
                System.out.println("testi 44");
                speakOut();
            }


        } else {
            Log.e("TTS", "Initilization Failed");
        }
    }


    private void speakOut() {

        String text = "Hey, it is done";
        System.out.println("testi41");

        tts.speak(text, TextToSpeech.QUEUE_FLUSH, new Bundle(), "uniiikkiyks");
    }
}
